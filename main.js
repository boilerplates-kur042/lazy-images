// Here threshold: 1 means that we launch the callback when the elt is showed at 1 => 100%
const options = {
    threshold: 1
}

const observer = new IntersectionObserver(imageObserver, options);

function imageObserver(entries, observer) {
   entries.forEach( entry => {
       if (entry.isIntersecting) {
           const img = entry.target;
           const img_src = entry.target.dataset.src;
           img.src = img_src;
           console.log(img_src);
           // To prevent it from launching every single time, we remove the image from being observed
           observer.unobserve(img);
       }
   });
}

let imgs = document.querySelectorAll('img.lazy');

imgs.forEach( img => {
    observer.observe(img)
});